PRODUCT = rdos
VERSION = $(shell git log -1 --format=%cd --date=format:%Y%m%d)

PROGRAMS = hello.com sys.com
KERNEL = @$(PRODUCT).com
DISTFILES = $(KERNEL) $(PROGRAMS)
FLOPPY = 1440

# Programs used for building
AS = host/as
SYS = ./sys.elf

.PHONY: default clean qemu
.PRECIOUS: %.com

default: fd$(FLOPPY).img

# Host utils
%.elf: host/%.c
	$(CC) -o $@ $<

# COM programs
%.com: src/%.asm src/*.inc
	$(AS) $@ $<
#	$(NASM) $(NASM_ARGS) -Isrc -l $(@:.com=.lst) -o $@ $<

# Bootloader images
%.bs: boot/%.asm
	$(AS) $@ $<

fd%.img: $(DISTFILES) $(SYS)
	mformat -C -i $@ -f $* -v "$(PRODUCT) $(VERSION)" ::
	mcopy      -i $@ $(DISTFILES) ::
	$(SYS)        $@ $(KERNEL)

clean:
	rm -f *.com *.bs *.0 *.lst *.img *.bin *.hex *.elf

qemu: fd$(FLOPPY).img
	qemu-system-i386 $(shell test -w /dev/kvm && echo --enable-kvm) $(shell test -n "$DISPLAY" || echo --display curses) -boot a -drive file=fd$(FLOPPY).img,format=raw,index=0,if=floppy
