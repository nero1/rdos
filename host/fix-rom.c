/* QEMU and x86 hardware with PCI support (and up) require that BIOS options
 * rom have a valid checksum, that the sum of all their bytes equals zero.
 * This script edits a rom binary in-place so it fits that criteria.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>

struct stat sbuf;

int main(int argc, char** argv) {
  FILE* fd = fopen(argv[1], "r+");
  fstat(fileno(fd), &sbuf);
  if (sbuf.st_size & 0x1F) {
    fprintf(stderr, "Filesize is not a multiple of 512 bytes\n");
    exit(1);
  }

  // Fill out filesize flag
  fseek(fd, 2, SEEK_SET);
  fputc(sbuf.st_size >> 9, fd);

  // Calculate checksum
  fseek(fd, 0, SEEK_SET);
  off_t i;
  uint8_t s;
  for (i=0; i<sbuf.st_size; i++) {
    s+=fgetc(fd);
  }

  // Edit last byte so that checksum is 0
  fseek(fd, -1, SEEK_END);
  s=fgetc(fd)-s;
  fseek(fd, -1, SEEK_END);
  fputc(s, fd);

  fclose(fd);
}
