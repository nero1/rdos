## Primary goals

- Run on 8086
- Be able to load and execute a COM program from disk
- Self-hosting assembler

## Building

Build requirements:

- make
- busybox awk for bootstrap assembler
- mtools >=4.0.20 (Debian buster or up)

Useful for testing:

- C compiler
- qemu-system-i386
