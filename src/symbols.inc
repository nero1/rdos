; implements a 6-byte symbol table
; starts at s_end and grows down from there
; all functions require DS=+CS

s_start dw 0xC000 ; decreasing with usage
s_end	dw 0xC000 ; const

; incremented on freeing an entry
; so we know if its worth searching
s_gaps	db 0

; allocate a symbol table entry
; OUT	bx	ptr to entry
s_alloc	mov bx, [s_start]
	; check if its worth searching for a gap
	cmp byte [s_gaps], 0
	je .l04
.l02	cmp bx, [s_end]
	jnc .l01
	; test if unallocated
	cmp byte [bx], 0
	jne .l03
	; we found one! take note
	dec byte [s_gaps]
	jmp .l05
.l03	add bx, 8
	jmp .l02
	; no gap found :/
.l01	mov byte [s_gaps], 0
	; extend symbol table at bottom
.l04	mov bx, [s_start]
	sub bx, 8
	mov [s_start], bx
	; insert default values
.l05	mov [bx], dx
	mov [bx+2], si
	mov [bx+4], di
	mov [bx+6], cx
	clc
	ret

; free a symbol table entry
; IN	bx	ptr to entry
s_free	mov byte [bx], 0
	inc byte [s_gaps]
	ret

; compare a table entry by string val
; IN	bx	ptr to entry
;	dx:si:di 6-byte ascii name, space padded
; OUT	zero flag set if match
s_comp	mov ax, [bx]
	and ax, 0x7F7F
	cmp ax, dx
	jne .l01
	mov ax, [bx+2]
	and ax, 0x7F7F
	cmp ax, si
	jne .l01
	mov ax, [bx+4]
	and ax, 0x7F7F
	cmp ax, di
.l01	ret

; fill in a backreference
; IN	bx	ptr to tab entry
;	cx	value
s_bref	push bx
	mov bx, [bx+6]
	; todo: handle 8-bit relative backrefs
	;add [output+bx], cx
	pop bx
	jmp s_free

; set a symbol
; IN	dx:si:di 6-byte ascii name, space padded
;	cx	 value
s_set	mov bx, [s_start]
.loop	cmp bx, [s_end]
	jnc .new
	cmp byte [bx], 0
	je .next
	call s_comp
	jne .l01
	test byte [bx], 0x80
	jnz .bref
	stc
	ret
;	call s_prnt
;	call errmsg
;	db "DUP", 0
.bref	call s_bref
	jmp .next
.l01	cmp dl, '.'
	je .next
	cmp byte [bx], '.'
	jne .next
	; delete local symbols if current set is non-local
	call s_free
.next	add bx, 8
	jmp .loop
.new	jmp s_alloc

; get value of symbol
; IN	dx:si:di 6-byte label
; OUT 	carry	clear if label exists
;	cx	symbol value, or 0 if not found
s_get	mov bx, [s_start]
.loop	cmp bx, [s_end]
	jnc .fail
	call s_comp
	jnz .next
	; skip if backref
	test byte [bx], 0x80
	jnz .next
	mov cx, [bx+6]
	clc
	ret
.next	add bx, 8
	jmp .loop
.fail	mov cx, 0
	stc
	ret

; print out a table entry
; IN	bx	ptr to entry
s_prnt	mov dx, [bx]
	call putc
	mov dl, dh
	call putc
	mov dx, [bx+2]
	call putc
	mov dl, dh
	call putc
	mov dx, [bx+4]
	call putc
	mov dl, dh
	call putc
	mov dl, ' '
	call putc
	mov ax, [bx+6]
	mov bx, 0x10
	mov cx, 4
	call putnum
	jmp newlin

; print out the full symbol table
s_dump	mov bx, [s_start]
.loop	cmp byte [bx], 0
	je .next
	push bx
	call s_prnt
	pop bx
.next	add bx, 8
	cmp bx, [s_end]
	jc .loop
	ret
