; BIOS parameter block
; We use DOS 3.31 version as common denominator

; Offset into boot sector
BPBOFF	equ 0xB

BPBSS	equ 0 ; word bytes per sector
BPBSC	equ 2 ; byte sectors per cluster
BPBRSC	equ 3 ; word reserved sector count
BPBFN	equ 5 ; byte number of fat tables
BPBRDE	equ 6 ; word root disk entries
BPBTS	equ 8 ; word total sectors
BPBMD	equ 10 ; byte media descriptor byte
BPBFS	equ 11 ; word sectors per fat
BPBSPT	equ 13 ; word sectors per track
BPBNOS	equ 15 ; word number of sides/heads
BPBHS	equ 17 ; dword partition offset
BPBLTS	equ 21 ; dword large total sectors
BPBSIZ	equ 25 ; size in bytes
