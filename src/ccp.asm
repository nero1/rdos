fcb1	equ 0x5C
fcb2	equ 0x6C
buf	equ 0x80

	; length of input buffer
inlen	equ 0x90

	org 0x100

	jmp prompt

banner	db "RDOS Console Command Processor",0x0A,0x0D,0x1A

%include "print.inc"
%include "scan.inc"

	; eat leftover spaces
	; IN	ds:si	ptr
eatws	jmp .l01
.l02	inc si
.l01	cmp byte [si], 0x20
	je .l02
	ret

prompt	mov ax, cs
	mov ds, ax
	mov es, ax
	; print newline
	mov dl, 0x0A
	mov ah, 2
	int 0x21
	mov dl, 0x0D
	int 0x21
	; query current drive
	mov ah, 25
	int 0x21
	; drivenum to chr
	mov dl, al
	add dl, 0x41
	; print drive and prompt chr
	mov ah, 2
	int 0x21
	mov dl, '>'
	int 0x21
	; read string
	mov byte [inbuf], inlen
	mov dx, inbuf
	mov ah, 10
	int 0x21
	cmp byte [inbuf+1], 0
	je prompt
	mov dl, 0x0A
	mov ah, 2
	int 0x21
	mov dl, 0x0D
	int 0x21

	; parse inbuf into comfcb, buf, fcb1 and fcb2
	; takes no arguments
	; parse off command name
parse	mov si, inbuf+2
	mov di, comfcb
	call lodfn
	; trim spaces
	call eatws
	; calculate length of cmdline
	mov cx, inbuf+2
	add cl, [inbuf+1]
	adc ch, 0
	sub cx, si
	; move into default position at 0x80
	mov [buf], cl
	mov di, buf+1
	rep movsb
	mov al, 0x0D
	stosb
	; parse fcb1
	mov si, buf+1
	mov di, fcb1
	call lodfn
	; fill space between with zeroes
	add di, 12
	mov cx, fcb2-fcb1-12
	mov al, 0
	rep stosb
	; parse fcb2
	call eatws
	mov di, fcb2
	call lodfn
	; fill space between with zeroes
	add di, 12
	mov cx, buf-fcb2-12
	mov al, 0
	rep stosb
	; dump out everything parsed for me to see
	;mov si, comfcb
	;call dump
	;mov si, fcb1
	;call dump
	;mov si, fcb2
	;call dump
	;mov si, buf
	;call dump
	call exec
	jmp prompt

	; execute, prog in comfcb, args in buf
exec	mov bx, ctab
	jmp .l02
.l01	add bx, 10
	cmp byte [bx], 0
	je .ret
.l02	mov si, comfcb+1
	mov di, bx
	mov cx, 4
	repe cmpsw
	jne .l01
	jmp [bx+8]
.ret	mov dx, enoent
	mov ah, 9
	int 0x21
	ret

enoent	db "no command", 0x0A, 0x0D, "$"

ctab	db "DIR     "
	dw c_dir
	db "TYPE    "
	dw c_type
	db 0

c_dir	mov di, fcb1+1
	cmp byte [di], 0x20
	jne .l01
	mov ax, '??'
	mov cx, 4
	rep stosw
.l01	mov di, fcb1+9
	cmp byte [di], 0x20
	jne .l02
	mov al, '?'
	mov cx, 3
	rep stosb
.l02	mov dx, buf
	mov ah, 0x1A
	int 0x21
	mov ah, 0x11 ; find first
.loop	mov dx, fcb1
	int 0x21
	test al, al
	jnz .ret
	mov dl, [buf]
	add dl, 0x40
	mov ah, 2
	int 0x21
	mov dl, ':'
	int 0x21
	mov cx, 8
	mov si, buf+1
.l03	lodsb
	mov dl, al
	int 0x21
	loop .l03
	mov dl, ' '
	int 0x21
	mov cx, 3
	mov si, buf+9
.l04	lodsb
	mov dl, al
	int 0x21
	loop .l04
	mov dl, 0x0A
	int 0x21
	mov dl, 0x0D
	int 0x21
	mov ah, 0x12 ; find next
	jmp .loop
.ret	ret

	; set dta
c_type	mov dx, buf
	mov ah, 0x1A
	int 0x21
	; open file
	mov dx, fcb1
	mov ah, 0x0f ; fcb open
	int 0x21
	; fail
	test al, al
	jnz .err
	; place eof marker to read in err case
.loop	mov byte [buf], 0x1A
	; read a record
	mov dx, fcb1
	mov ah, 0x14 ; fcb seq read
	int 0x21
	; set up string read
	mov si, buf
	mov cx, 0x80
.l02	lodsb
	; break on eof or error
	cmp al, 0x1A
	je .ret
	; never write null bytes
	mov dl, al
	mov ah, 2
	int 0x21
	loop .l02
	jmp .loop
.ret	mov dx, fcb1
	mov ah, 0x10
	int 0x21
	ret
	; communicate failure to user
.err	mov dx, .errmsg
	mov ah, 9
	int 0x21
	ret
.errmsg db "no file", 0x0A,0x0D,"$"

section .bss

inbuf	resb inlen

	; FCB for the COM file
comfcb	resb 37
