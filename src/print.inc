	; print AX in base BX
	; destroys DX
putnum	xor dx, dx
	div bx
	test cx, cx
	loopnz .pad
	test ax, ax
	je .nib
.pad	push dx
	call putnum
	pop dx
	; print lower DL as digit
.nib	add dl, 0x30
	cmp dl, 0x3a
	jl putc
	add dl, 7
	; print DL as char
putc	mov ah, 2
	int 0x21
	ret

	; print str at SI
	; NUL-terminated
puts	lodsb
	test al, al
	jz .l01
	mov dl, al
	call putc
	jmp puts
.l01	ret

	; print str at return addr
	; NUL-terminated
putsh	pop si
	call puts
	jmp si

	; print a newline
newlin	mov dl, 0x0A
	call putc
	mov dl, 0x0D
	jmp putc

	; print register set
	; order AX CX DX BX BP SI DI IP
	; trashes flags
debug	push di
	push si
	push bp
	push bx
	push dx
	push cx
	push ax
	mov si, sp
	call dump
	pop ax
	pop cx
	pop dx
	pop bx
	pop bp
	pop si
	pop di
	ret

	; dump the 8 words at SI
dump	lea di, [si+16]
	mov bx, 0x10
.loop	lodsw
	mov cx, 4
	call putnum
	mov dl, ' '
	call putc
	cmp si, di
	jc .loop
	jmp newlin
