	jmp main

%include "print.inc"
%include "symbols.inc"

	; OUT	cx	value of '$' label
asmcur	mov cx, 0
	ret

	; IN	cx	runtime address
	; OUT	es:bx	address in binary
asmptr	mov bx, ds
	mov es, bx
	mov bx, cx
	add bx, buf
	ret

main	call putsh
	db "Normal read-back... ", 0

	mov dx, "LA"
	mov si, "BE"
	mov di, "L1"
	mov cx, 0x1234
	call s_set
	xor cx, cx

	call s_get
	cmp cx, 0x1234
	call testz

	ret

testz	jnz .fail
	call putsh
	db "OK", 0x0A, 0x0D, 0
	ret
.fail	call putsh
	db "FAIL", 0x0A, 0x0D, 0
	ret

buf	times 0x100 db 0
