; COM program displaying hello world string
; This is primarily for testing purposes
	org 0x0100

main:	mov si, string
	mov ah, 0x0e
	xor bx, bx
.loop:	lodsb
	test al, al
	jz .end
	int 0x10
	jmp .loop
.end:	ret

string:	db "Hello world!", 0x0A, 0x0D, 0
