	; make a char uppercase
	; IN	al	char
	; OUT	al	char
	; do nothing if >z
ucase	cmp al, 0x7B
	jnc .ret
	; do nothing if <a
	cmp al, 0x61
	jc .ret
	; do it!
	sub al, 0x20
.ret	ret

	; read integer from ascii string
	; default dec but allows 0x for hex
	; bogus result if larger than 16 bit
	; IN	ds:si	source string
	;	cx	max number of chars
	; OUT	ax	number
	;	ds:si	first rejected char
	;	cx	chars ignored
lodnum	xor di, di
	mov bx, 10
	cmp cx, 2
	jc .loop
	; test for 0x prefix
	mov ax, [si]
	cmp al, 0x30
	jne .loop
	mov al, ah
	call ucase
	cmp al, 0x58
	jne .loop
	; apply 0x prefix
.hex	add si, 2
	sub cx, 2
	mov bx, 0x10
	; check if there are chars left
.loop	test cx, cx
	jnz .do
.ret	mov ax, di
	ret
.do	mov al, [si]
	call ucase
	; check if in range
	sub al, 0x30
	jc .ret
	cmp al, 0x10
	jc .skip
	sub al, 7
	; must be smaller than base
	; necessary to prevent hex digits in decimals
.skip	mov ah, 0
	cmp ax, bx
	jnc .ret
	; mark as read
	inc si
	dec cx
	; add digit to number
	xchg ax, di
	mul bx
	add di, ax
	jmp .loop

lodfn	mov ah, al
	; set drive byte to default
	mov byte [es:di], 0
	; check if there is a drvspec
	mov al, [si+1]
	cmp al, ':'
	jne .nam
.drv	mov dx, 0x0100
	call .read
	inc si
	; adjust capital letter to number
	and byte [es:di], 0x1F
.nam	mov dx, 0x0901
	call .read
	mov al, [si]
	cmp al, '.'
	jne .ext
	inc si
.ext	mov dx, 0x0C09
	call .read
.ret	ret
	; read a padded string
.read	mov al, [si]
	mov bh, 0
	cmp dl, dh
	jnc .ret
	call ucase
	call fatchr
	jz .fill
	mov bl, dl
	mov [es:di+bx], al
	inc dl
	inc si
	jmp .read
	; fill padding with spaces
.fill	mov al, 0x20
.cfill	cmp dl, dh
	jnc .ret
	mov bl, dl
	mov [es:di+bx], al
	inc dl
	jmp .fill

	; is valid char for FAT filename
	;	IN	al	character
	;	OUT	al	char
	;		zf	clear if valid
fatchr	push cx
	push bx
	mov bl, al
	mov bh, 0
	mov cl, 3
	sar bx, cl
	mov ch, [cs:.vtab+bx]
	mov cl, al
	and cl, 7
	shr ch, cl
	test ch, 1
	pop bx
	pop cx
	ret
	; character validity map
	; 256 bits for each char
	; 1 = allowed in fat name
.vtab	dw 0x0000,0x0000 ; 00-1F control chars all forbidden
	dw 0x23FA,0x03FF ; 20-3F special and digits
	dw 0xFFFF,0xC7FF ; 40-5F uppercase
	dw 0x0001,0x6800 ; 60-7F lowercase
	dw 0x0000,0x0000 ; 80-9F
	dw 0x0000,0x0000 ; A0-BF
	dw 0x0000,0x0000 ; C0-DF
	dw 0x0000,0x0000 ; E0-FF
