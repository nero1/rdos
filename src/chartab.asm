	org 0x100

	mov ax, 0x0e20
	xor bx, bx
	int 0x10
	mov cx, 0x10
.hloop	mov al, 0x20
	int 0x10
	mov al, cl
	neg al
	add al, 0x10
	call btoa
	int 0x10
	loop .hloop
	mov al, 0x0A
	int 0x10
	mov al, 0x0D
	int 0x10

	mov cx, 0x20
.line	test cl, 0x0F
	jnz .loop
	mov al, cl
	sar al, 1
	sar al, 1
	sar al, 1
	sar al, 1
	and al, 0xF
	call btoa
	mov ah, 0x0e
	int 0x10
	mov al, 0x20
	int 0x10
.loop	mov al, cl
	int 0x10
	mov al, 0x20
	int 0x10
	mov al, cl
	inc cx
	and al, 0xF
	cmp al, 0xF
	jne .loop

	mov al, 0x0A
	int 0x10
	mov al, 0x0D
	int 0x10

	cmp cx, 0x100
	jc .line

	ret

btoa	add al, 0x30
	cmp al, 0x3A
	jc .ret
	add al, 7
.ret	ret
